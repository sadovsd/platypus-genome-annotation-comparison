# gene_id = "ENSOANG00000002544"

# Checking table metadata
DESCRIBE New_Exon;

# Get the first 3 rows from each table
SELECT * FROM New_Gene LIMIT 3;
SELECT * FROM New_Transcript LIMIT 3;
SELECT * FROM New_Exon LIMIT 3;

# Counting the number of rows in each table
SELECT count(*) FROM New_Gene;
SELECT count(*) FROM New_Transcript;
SELECT count(*) FROM New_Exon;
SELECT count(*) FROM Old_Gene;
SELECT count(*) FROM Old_Transcript;
SELECT count(*) FROM Old_Exone;

# Get gene annotation details for chosen gene from new assembly
SELECT gene_name, gene_biotype, chromosome, start_, end_, end_ - start_
FROM New_Gene
WHERE gene_id="ENSOANG00000002544";

# Final queries for new gene:
SELECT gene_name, gene_biotype, chromosome, start_, end_, end_ - start_ FROM New_Gene WHERE gene_id="ENSOANG00000002544";
SELECT transcript_id, transcript_name, start_, end_, end_ - start_ FROM New_Transcript WHERE gene_id="ENSOANG00000002544";
SELECT transcript_id, count(*) AS exon_count, count(*) + 1 AS intron_count FROM New_Exon WHERE gene_id="ENSOANG00000002544" GROUP BY transcript_id;

# Final queries for old gene:
SELECT gene_name, gene_biotype, chromosome, start_, end_, end_ - start_ FROM Old_Gene WHERE gene_id="ENSOANG00000002544";
SELECT transcript_id, transcript_name, start_, end_, end_ - start_ FROM Old_Transcript WHERE gene_id="ENSOANG00000002544";
SELECT transcript_id, count(*) AS exon_count, count(*) + 1 AS intron_count FROM Old_Exon WHERE gene_id="ENSOANG00000002544" GROUP BY transcript_id;

# Get the number of exons/introns for the chosen gene from new assembly
SELECT count(*) AS number_of_exons FROM New_Exon WHERE gene_id = "ENSOANG00000002544";
